% Partie I

% element_n(N, L, X)
element_n(1, [T|_], X) :- T = X, !.
element_n(N, [_|Q], X) :- NewN is N-1, element_n(NewN, Q, X).

% ligne_n(N, G, L)
ligne_n(1, [T|_], X) :- T = X, !.
ligne_n(N, [_|Q], X) :- NewN is N-1, ligne_n(NewN, Q, X).

% colonne_n(N, G, C)
colonne_n(_, [], []) :- !.
colonne_n(N, [T|Q], [Tnew|Qnew]) :- element_n(N, T, X), Tnew = X, colonne_n(N, Q, Qnew).

% colonnes_aux(G, Length, C)
colonnes_aux(_, 0, []) :- !.
colonnes_aux(G, Length, [T|Q]) :- colonne_n(Length, G, T), NewLength is Length-1, colonnes_aux(G, NewLength, Q).

% colonnes(G, C)
colonnes(G, C) :- element_n(1, G, X), length(X, Length), colonnes_aux(G, Length, C).

% Partie II

% suite_un(L, C)
suite_un([], 0).
suite_un([T|Q], C) :- T is 1, !, suite_un(Q, NewC), C is NewC+1.
suite_un(_, 0).

% get_passe_bloc(L, C, NewL)
get_passe_bloc(NewL, 0, NewL) :- !.
get_passe_bloc([_|Q], C, NewL) :- NewC is C-1, get_passe_bloc(Q, NewC, NewL).

% compte_un(L, NewL, Count)
compte_un([], NewL, 0) :- !, NewL = [].
compte_un([1|Q], NewL, Count) :- !, compte_un(Q, NewL, NewCount), Count is NewCount+1.
compte_un([0|Q], NewL, 0) :- !, NewL = Q. 

% compte_un_liste(L, NewL)
compte_un_liste([], []) :- !.
compte_un_liste(L, [Hnew|Qnew]) :- compte_un(L, NewL, Hnew), Hnew \= 0, !, compte_un_liste(NewL, Qnew).
compte_un_liste(L, Count) :- compte_un(L, NewL, _), compte_un_liste(NewL, Count).

% analyse_composante(C, B)
analyse_composante(C, B) :- compte_un_liste(C, NewC), NewC == B.

% analyse_l_composantes(LC, LB)
analyse_l_composantes([], []) :- !. 
analyse_l_composantes([HC|QC], [HB|QB]) :- analyse_composante(HC, HB), analyse_l_composantes(QC, QB).

% Partie III

% genere_bloc(X, T, B)
genere_bloc(_, 0, []) :- !.
genere_bloc(X, T, [H|Q]) :- H = X, NewT is T-1, genere_bloc(X, NewT, Q).

% genere_ligne_aux(N, L)
genere_ligne_aux(0, []) :- !.
genere_ligne_aux(N, [1|Q]) :- NewN is N-1, genere_ligne_aux(NewN, Q).
genere_ligne_aux(N, [0|Q]) :- NewN is N-1, genere_ligne_aux(NewN, Q).

% genere_ligne(N, LB, L)
genere_ligne(N, LB, L) :- genere_ligne_aux(N, NewL), analyse_composante(NewL, LB), NewL = L.

% genere_grille(N, BS, LS)
genere_grille(_, [], []) :- !.
genere_grille(N, [H|Q], [HS|QS]) :- genere_ligne(N, H, HS), genere_grille(N, Q, QS).

% solve_picross_aux(LCC, LCL, G)
solve_picross_aux(LCC, LCL, G) :- colonnes(LCL, NHCL), LCC == NHCL, !, G = NHCL.

% solve_picross(CL, CC, G)
solve_picross(CL, CC, G) :- length(CL, LCL), length(CC, LCC), genere_grille(LCC, CL, NCL), genere_grille(LCL, CC, NCC), solve_picross_aux(NCL, NCC, G).